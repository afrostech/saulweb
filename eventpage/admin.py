from django.contrib import admin

from .models import TeamMember


class MemberAdmin(admin.ModelAdmin):
    list_display = ('first_name', 'surname', 'role')
admin.site.register(TeamMember, MemberAdmin)
