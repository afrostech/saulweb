import traceback
from django import forms
from eventpage.models import TeamMember


class contact_form(forms.Form):
    name = forms.CharField()
    message = forms.CharField()
    subject = forms.CharField(required=False)
    reply_email = forms.EmailField()
    auto_submit = forms.CharField(required=False, max_length=0)


class subscribe_form(forms.Form):
    name = forms.CharField(required=False)
    email = forms.EmailField()
    auto_submit = forms.CharField(required=False, max_length=0)


class MemberForm(forms.ModelForm):
    class Meta:
        model = TeamMember
        fields = ['first_name', 'surname', 'role', 'rank', 'picture_link', 'picture_tag', 'bio']

    def __init__(self, data, updating=False, **kwargs):
        super(MemberForm, self).__init__(data, **kwargs)
        if updating:
            # self.fields['update'] = forms.BooleanField()
            # self.fields['update'].initial = True
            for key, field in self.fields.items():
                try:
                    if not field.widget.value_from_datadict(self.data, self.files, self.add_prefix(key)):
                        self.fields[key].required = False
                except AttributeError:
                    print(traceback.format_exc())
