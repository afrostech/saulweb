from __future__ import unicode_literals

from django.db import models

class TeamMember(models.Model):
    first_name = models.CharField(max_length=99)
    surname = models.CharField(max_length=99)
    role = models.CharField(max_length=99, null=True, blank=True)
    picture_link = models.URLField(blank=True)
    picture_tag = models.CharField(max_length=99, blank=True)
    bio = models.CharField(max_length=200, null=True, blank=True)
    rank = models.IntegerField(default=50)
