import bsGallery from 'bootstrap-image-gallery';
import gallery from 'blueimp-gallery';
// extend $ with new properties
bsGallery($, gallery);

$(document).ready(function(){
    $('#launchGallery').on('click', function (event) {
        event && event.preventDefault ? event.preventDefault() : 
                                        event.returnValue = false;
       gallery($('#links a'), $('#blueimp-gallery').data())
      })
});