if (!String.prototype.format) {
    String.prototype.format = function () {
        var args = arguments;
        var str = this;

        function replaceByObjectProperies(obj) {
            for (var property in obj)
                if (obj.hasOwnProperty(property))
                    //replace all instances case-insensitive
                    str = str.replace(new RegExp(escapeRegExp("{" + property + "}"), 'gi'), String(obj[property]));
        }

        function escapeRegExp(string) {
            return string.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
        }

        function replaceByArray(arrayLike) {
            for (var i = 0, len = arrayLike.length; i < len; i++)
                str = str.replace(new RegExp(escapeRegExp("{" + i + "}"), 'gi'), String(arrayLike[i]));
        }

        if (!arguments.length || arguments[0] === null || arguments[0] === undefined)
            return str;
        else if (arguments.length == 1 && Array.isArray(arguments[0]))
            replaceByArray(arguments[0]);
        else if (arguments.length == 1 && typeof arguments[0] === "object")
            replaceByObjectProperies(arguments[0]);
        else
            replaceByArray(arguments);

        return str;
    };
}