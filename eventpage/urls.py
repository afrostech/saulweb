from django.conf.urls import url
import eventpage.views as views


urlpatterns = [
    url(r'^$', views.home, name='saul'),
    url(r'^organisation/$', views.OrgView.as_view(), name='org'),
    url(r'^notre_universite/$', views.presentation_ul, name="ulaval"),
    url(r'^gallery/$', views.gallery, name='gallery'),
    url(r'^contact/$', views.contact, name='contact'),
    url(r'^424e388901/subscribe/$', views.subscribe, name='suscribe'),
    url(r'^(?P<list_id>\w+)/subscribe/$', views.subscribe, name='suscribe_custom'),
    url(r'^intranet/login/$', views.login_intranet, name='intranet_login'),
    url(r'^intranet/$', views.manage_members, name='intranet'),
    #url(r'^saul/news/$', views.news, name='news'),
]
