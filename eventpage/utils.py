from django.core.mail import EmailMultiAlternatives
import mailchimp
import os
import datetime
import json
import cloudinary

cloudinary.config(
    cloud_name = "csaul",
    api_key = "741718464753277",
    api_secret = "Afhk-asWc3S7kCNGWV5TiCqAz8A",
    secure = True
)

def get_mailchimp_api():
    return mailchimp.Mailchimp('1cce85ef633eeec39d703e4fb26d0ac8-us12') #your api key here


def send_mail(subject, message, from_email, recipient_list,
              fail_silently=False, auth_user=None, auth_password=None,
              connection=None, html_message=None, reply_to=None):
    """
    Easy wrapper for sending a single message to a recipient list. All members
    of the recipient list will see the other recipients in the 'To' field.

    If auth_user is None, the EMAIL_HOST_USER setting is used.
    If auth_password is None, the EMAIL_HOST_PASSWORD setting is used.

    Note: The API for this method is frozen. New code wanting to extend the
    functionality should use the EmailMessage class directly.
    """

    mail = EmailMultiAlternatives(subject, message, from_email, recipient_list, reply_to=reply_to)
    if html_message:
        mail.attach_alternative(html_message, 'text/html')

    return mail.send()

# is file-stored json data outdated ?

def is_store_valid(store_name, lifetime):
    try:
        with open(store_name, 'r') as f:
            store = json.load(f)
            if datetime.datetime.utcnow() - modification_date(f.name) < datetime.timedelta(hours=lifetime):
                return store
            else:
                valid = False
    except IOError:
        valid = False
    return valid

# manipulating file modification dates

def modification_date(filename):
    t = os.path.getmtime(filename)
    return datetime.datetime.utcfromtimestamp(t)

def delta_dates(date1, date2, diff=None):
    if diff:
        return (date1 - date2 > diff) if (date1 > date2) else (date2 - date1 > diff)
    return date1 - date2