# -*- coding: UTF-8 -*-
import os
import json
import mailchimp
import cloudinary
import cloudinary.api
import datetime
from django.http import HttpResponse, Http404
from django.shortcuts import render, redirect
from django.urls import reverse
from django.core.mail import BadHeaderError
from django.views.generic.list import ListView
from django.views.decorators.clickjacking import xframe_options_exempt
from django.utils.decorators import method_decorator
from django.forms.models import model_to_dict
from django.conf import settings
from django.contrib.staticfiles.templatetags.staticfiles import static

from eventpage.models import TeamMember
from eventpage.forms import MemberForm, contact_form, subscribe_form
from eventpage.utils import get_mailchimp_api, send_mail, modification_date


cloudinary.config(
    cloud_name="csaul",
    api_key="741718464753277",
    api_secret="Afhk-asWc3S7kCNGWV5TiCqAz8A",
    secure=True
)


@xframe_options_exempt
def home(request):
    ctx = {
        'ishome': 'active', 
        'partners': {}}
    try:
        pth = os.path.join(os.path.dirname(os.path.abspath(__file__)),
                           'static', 'eventpage', 'images', 'partners', 'partners.json')
        with open(pth, 'r') as f:
            ctx['partners'].update(json.load(f))
    except IOError:
        pass
    return render(request, 'eventpage/home.html', ctx)


@xframe_options_exempt
def presentation_ul(request):
    return render(request, 'eventpage/presentation_ulaval.html', {'isulaval': 'active'})


@method_decorator(xframe_options_exempt, name="dispatch")
class OrgView(ListView):
    model = TeamMember
    paginate_by = 6
    page_kwarg = 'group'

    def get_context_data(self, **kwargs):
        context = super(OrgView, self).get_context_data(**kwargs)
        return context

    def get_template_names(self):
        templates = super(OrgView, self).get_template_names()
        templates.append('eventpage/organisation.html')
        return templates

    def get_ordering(self):
        return ['rank', 'role']


@xframe_options_exempt
def gallery(request):
    images_store = {}
    try:
        with open(os.path.join(settings.BASE_DIR, 'store_gallery'), 'r') as f:
            images_store = json.load(f)
            if datetime.datetime.utcnow() - modification_date(f.name) < datetime.timedelta(hours=5):
                valid = True
            else:
                valid = False
    except IOError:
        valid = False
    if valid:
        images = cloudinary.api.resources(type='upload')['resources']
        images_store['dabalidrome'] = []
        for image in images:
            if 'dabalidrome' in image['public_id']:
                images_store['dabalidrome'].append(image['secure_url'])
                with open(os.path.join(settings.BASE_DIR, 'store_gallery'), 'w') as f:
                    json.dump(images_store, f)
    if not images_store:
        images_store = settings.GALLERY
    return render(request, 'eventpage/gallery.html', {'isgallery': 'active', 'gallery_store': images_store})


@xframe_options_exempt
def contact(request):
    if request.method == 'POST':
        new_contact = contact_form(request.POST)
        alert = {'type': 'danger', 'text': 'Look at your entries, something is not right...'}
        if new_contact.is_valid():
            message = new_contact.cleaned_data['message']
            name = new_contact.cleaned_data['name']
            subject = new_contact.cleaned_data.get('subject', 'No subject')
            reply_email = new_contact.cleaned_data['reply_email']
            sender = 'contact@semaineafricaineul.ca'
            recipient = 'comite@semaineafricaineul.ca'
            message = 'Envoyé par: {}\n \n \n {}'.format(name, message)
            try:
                result = send_mail(subject, message, from_email=sender, recipient_list=[recipient, ],
                                   reply_to=[reply_email, ])
            except BadHeaderError:
                pass
            success = True
            contact_message = 'Votre message a été bien envoyé.'
        else:
            success = False
            contact_message = "Votre message n'a pas été envoyé."
        if request.is_ajax:
            data = {'contact_response': True, 'contactSuccess': success, 'contactMessage': contact_message}
            return HttpResponse(json.dumps(data), content_type='application/json')
        else:
            return render(request, 'eventpage/home.html', {'contact_response': True,
                                                           'contact_success': success,
                                                           'contact_message': contact_message})
    return redirect(reverse('saul'))


@xframe_options_exempt
def subscribe(request, list_id='424e388901'):
    new_subscriber = subscribe_form(request.POST)
    if new_subscriber.is_valid():
        try:
            m = get_mailchimp_api()
            m.lists.subscribe(list_id, {'email': new_subscriber.cleaned_data.get('email', '')})
            success = True
            message = 'Super, merci de vous être enregistré.'
        except mailchimp.ListAlreadySubscribedError:
            message = "Vous êtes déjà enregistré dans l'infolettre."
            success = False
        except mailchimp.Error as e:
            message = "il y a eu une erreur, réessayez plus tard."
            success = False
    else:
        success = False
        message = 'Information invalide !'
    if request.is_ajax:
        data = {'subscribeResponse': True, 'subscribeSuccess': success, 'subscribeMessage': message}
        return HttpResponse(json.dumps(data), content_type='application/json')
    return render(request, 'eventpage/subscribe.html', {'subscribe_success': success,
                                                        'subscribe_message': message})


def login_intranet(request):
    alert = None
    authorized = request.session.get('state_logged', False)
    if authorized:
        return redirect(reverse('intranet'))
    elif request.POST.get('notsomeone', False):
        raise Http404
    elif request.POST.get('password') == 'SaulTeam@2016':
        request.session['state_logged'] = True
        print('thru login')
        return redirect(reverse('intranet'))
    elif request.POST.get('password'):
        alert = "Vous êtes sûr que vous êtes dans ma clique ?"

    return render(request, 'eventpage/intranet.html', {'alert': alert})


def manage_members(request):
    if request.method == 'POST':
        try:
            updated_member = TeamMember.objects.get(id=request.session.get('id', None))
            print(updated_member.id)
        except:
            updated_member = None

        if updated_member or (not updated_member and request.session.get('create_member', False)):
            data = request.POST
            updating = True if updated_member else False
            updated_member = MemberForm(data, updating=updating, instance=updated_member)
            if updated_member.is_valid():
                updated_member.save()
                print('OK')
            else:
                print(updated_member.errors)
    elif request.method == 'GET':
        request.session['id'] = request.GET.get('member_selected', None)

    authorized = request.session.get('state_logged', False)
    if authorized:
        try:
            selected = TeamMember.objects.get(id=request.session.get('id', None))
        except TeamMember.DoesNotExist:
            selected = None
    else:
        selected = None
    return render(request, 'eventpage/intranet.html', {'authorized': authorized,
                                                       'members': TeamMember.objects.all(),
                                                       'member': selected})


def create_member(request):
    request.session['create_member'] = True
    return manage_members(request)


def news(request):
    return render(request, 'eventpage/newsletter.html')
