module.exports = function(grunt) {
	var staticrootJs = 'eventpage/static/eventpage/js/';
	var staticrootCss = 'eventpage/static/eventpage/css/';
	var thtml = 'served/initial/';
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		uncss: {
		  dist: {
			src: [
				thtml+'home.html',
				thtml+'organisation.html',
				thtml+'gallery.html'
			],
			dest: 'served/final/css/main.min.css',
			options: {
			  ignore: ['.top-nav-collapse', '/[^}]*.open/g','.waves-ripple'],
			  report: 'min' // optional: include to report savings
			}
		  }
		},
		uglify: {
			options: {
			  mangle: true
			},
			my_target: {
			  files: {
			    'served/final/js/main.min.js': [
			    	'eventpage/static/eventpage/js/mdb.js',
			    	staticrootJs+'overridepure.js',
			    	staticrootJs+'countdown-timer.js',
			    	staticrootJs+'smooth-scroll.js',
			    	staticrootJs+'validate.min.js'
			    ]
			  }
			}
		},
	});

	grunt.loadNpmTasks('grunt-uncss');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.registerTask('default' , ' ' , function(){
		grunt.log.write('This grunt task is pointless !');
	});  
};