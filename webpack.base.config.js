var path = require("path");
var fs = require('fs');
var webpack = require('webpack');
var glob = require('fast-glob');
var entries = require('./webpack-entries-dirs.js');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var buildPath = path.resolve(__dirname, './static/bundles/dev/');
var nodeModulesPath = path.resolve(__dirname, 'node_modules');

module.exports = {
  context: __dirname,

  entry: function(){
       let patterns = {
          main: [
            'eventpage/css/customicons.css',
            'css/mdb.css',
            'eventpage/css/main.css',
            'eventpage/css/style.css',

            'js/mdb.js',
            'eventpage/js/overridepure.js'
          ],

          gallery: [
            'css/blueimp-gallery.css',
            'css/bootstrap-image-gallery.min.css',

            'eventpage/js/gallery.js'
          ]
        };
       let matches = {}; 
       Object.keys(patterns).map(function(k){
          matches[k] = entries(patterns[k]);
       });
       console.log(matches);
       return matches
  }, // if using directories as `entry`, they should have a `index.js` file that require all the assets used.

  output: {
    filename: '[name].js',
    path: buildPath
  },

  devtool: 'eval',

  plugins: [
    new ExtractTextPlugin({
        filename: '[name].css',
        allChunks: true
    }),
    new webpack.ProvidePlugin({
        "window.jQuery": "jquery",
        "jQuery": "jquery",
        "$": "jquery",
        AOS: "aos",
    })
  ],

  module: {
    //Loaders to interpret non-vanilla javascript code as well as most other extensions including images and text.
    rules: [
      {
        test: /\.css$/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: [
            {loader: 'css-loader', options: {importLoaders: 1}},
            'postcss-loader'
          ]
        })
      },
      {
        test: /fonts[\\\/].+\.(eot|ttf|otf|woff|woff2|svg)(?:\?v=\d+\.\d+\.\d+|[a-z0-9]+)?$/,
        use: {
            loader: 'url-loader',
            options: {
                // if size < 10kb use dataURL
                limit: 10000,
                name: 'fonts/[name].[ext]',
                publicPath: './',
            }
        },
      },
      {
        test: /\.(png|gif|jpg|jpeg|svg)$/,
        use: {
          loader: 'url-loader',
          options: {
              // if size < 8kb use dataURL
              limit: 8192,
              name: 'img/[hash]-[name].[ext]',
              publicPath: './'
          }
        }
      },
      { test: /\.json$/, loader: 'json' },
    ]
  },

  resolve: {
    alias: {
        jquery: path.resolve(__dirname, './static/vendors/js/jquery.min.js'),
        "blueimp-gallery": path.resolve(__dirname, './static/vendors/js/blueimp-gallery.js'),
        "bootstrap-image-gallery": path.resolve(__dirname, './static/vendors/js/bootstrap-image-gallery.js')
    },
    modules: [
        'node_modules',
    ],
    extensions: ['.js', '.jsx', 'json']
  },
}