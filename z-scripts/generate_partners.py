import os
import json
import functools


if __name__ == '__main__':
    pth = os.path.join(
        os.path.dirname(os.path.dirname(os.path.abspath(__file__))),
        'eventpage', 'static', 'eventpage', 'images', 'partners')
    get_partners = lambda walk: (lambda imglist: {
                    img.split('.')[0]: {
                        'img': img, 'name': ' '.join(img.split('.')[0].split('_'))}
                        for img in imglist})(walk[2])
    result = functools.reduce(lambda x,y: x.update(y) or x, map(get_partners, os.walk(pth)), {})
    with open(os.path.join(pth, 'partners.json'), 'w') as f:
        json.dump(result, f)
    