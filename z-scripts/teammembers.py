# -*- coding:utf-8 -*-
from django.core.exceptions import MultipleObjectsReturned
from eventpage.models import TeamMember
import os


COMITE = {
    'josias': {'fname': 'Josias', 'sname': 'Angoran', 'role' : '','link': ''},
    'malika': {'fname': 'Malika Sarah', 'sname': 'Coulibaly', 'link': ''},
    'rosie': {'fname': 'Rosie', 'sname': 'Kasongo', 'link': ''},
    'steeven': {'fname': 'Eddy Steven', 'sname': 'Drigba', 'link': ''},
    'louise': {'fname': 'Louise', 'sname': '', 'link': ''},
    'vanessa': {'fname': 'Vanessa', 'sname': 'Kalala', 'link': ''},
    'lindsay': {'fname': 'Lindsay', 'sname': 'Gueï', 'link': ''},
    'marvin': {'fname': 'Marvin', 'sname': 'Adjovi', 'link': ''},
    'eunice': {'fname': 'Eunice', 'sname': 'Faïzoun', 'link': ''},
    'yvan_d': {'fname': 'Yvan', 'sname': 'Dogbolot', 'link': ''},
    'yvan_aka': {'fname': 'Yvan', 'sname': 'Aka', 'link': ''},
    'lise': {'fname': 'Lise', 'sname': 'Manirakiza', 'link': ''},
    'marie_renée': {'fname': 'Marie-Renée', 'sname': 'Faye', 'link': ''},
    'bouba': {'fname': 'Aboubacar', 'sname': 'Karim', 'link': ''},
    'rebecca': {'fname': 'Rebecca', 'sname': 'Kossa', 'link': ''},
    'ingrid': {'fname': 'Ingrid', 'sname': 'Angoua', 'link': ''},
    'inès': {'fname': 'Inès', 'sname': 'Ribeiz', 'link': ''},
}


def fire_up():
    for key, member in COMITE.items():
        TeamMember(first_name = member['fname'], surname = member['sname'],
                   picture_link = member['link'], picture_tag = key, role = member['role'] if 'role' in member.keys() else '',
                   bio = member['bio'] if 'bio' in member.keys() else '').save()


def update(**kwargs):
    for key, value in kwargs.items():
        print value
        for name, data in value.items():
            try:
                a = TeamMember.objects.get(first_name = name)
            except MultipleObjectsReturned:
                found = TeamMember.objects.filter(first_name = name)
                a = None
                for record in found:
                    if key == 'picture_tag':
                        if data.lower().find(record.surname.replace(u'é', 'e').replace(u'è','e').lower()) != -1:
                            a = record
                            break
                    a = None
            if a:
                setattr(a, key, data)
                a.save()


def update_picture_tags():
    picture_tags = {}
    picture_location = 'C:\\Users\\konoufo\\Downloads\\CSA\\comite\\web'
    for img in os.listdir(picture_location):
        if img.lower().endswith('jpg') or img.lower().endswith('png'):
            for entries in COMITE.values():
                fname = entries['fname']
                if img.lower().find(fname.lower().replace('è', 'e').replace('é', 'e')) != -1:
                    print img
                    try:
                        picture_tags[fname] = u''.join(unicode(img, 'utf-8').encode().split(u'.')[:-1])
                    except UnicodeError:
                        picture_tags[fname] = u''.join(unicode(img, 'utf-8').split('.')[:-1])
    update(picture_tag=picture_tags)


def show_tags():
    for member in TeamMember.objects.all():
        print u'{name} {tag}'.format(name=member.surname, tag=member.picture_tag)


if __name__ == '__main__':
    k = 0
    boto_thougts = 'Hi, i''m boto ! You know what to do ? '
    while True:
        k+=1
        try:
            boto_q = raw_input(boto_thougts)
        except KeyboardInterrupt:
            print 'BYE !!!'
            break

        if boto_q == 'tags':
            update_picture_tags()
            boto_thougts = 'We played {} {} since we started. What now ?'.format(k, 'time' if k<2 else 'times')
        elif boto_q == 'bye':
            break

    print u'bye shônen !'
else:
    update_picture_tags()
    show_tags()