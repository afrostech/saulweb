# -:- coding:utf-8 -:-
from eventpage.models import TeamMember
from eventpage.utils import cloudinary
import cloudinary.api
from django.conf import settings


def update_all():
    try:
        k = 0
        for member in TeamMember.objects.all():
            if member.picture_tag:
                try:
                    member.picture_link = cloudinary.api.resource('mediafiles/comite/{}'.format(member.picture_tag))['secure_url']
                    member.save()
                    k+=1
                except (cloudinary.api.GeneralError, cloudinary.api.NotFound):
                    print member.picture_tag

        print u'DONE ! {} links modified.'.format(k).decode('utf-8')
    except TeamMember.DoesNotExist:
        print "No team members registered"
        pass


def delete(path):
    if path=='comite':
        cloudinary.api.delete_resources_by_prefix('mediafiles/comite/')

update_all()